/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.demo.osgi.remote.service.admin.cxf.provider.internal;

import fr.matoeil.demo.osgi.remote.service.admin.cxf.api.EchoService;
import org.osgi.service.component.annotations.Component;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Component(configurationPid = "", property = {
        "service.exported.interfaces=fr.matoeil.demo.osgi.remote.service.admin.cxf.api.EchoService",
        "service.exported.configs=org.apache.cxf.ws",
        "org.apache.cxf.ws.address=http://localhost:8181/dosgi/fr/matoeil/demo/osgi/remote/service/admin/cxf/api/EchoService",
        "org.apache.cxf.ws.httpservice.context=/dosgi",
        "osgi.remote.configuration.httpservice.context=/dosgi"
})
public class SimpleEchoService implements EchoService {

    @Override
    public String doEcho(final String aMessage) {
        return aMessage;
    }
}
